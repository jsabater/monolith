import ROOT
from ROOT import gSystem, gROOT, TCanvas, TGraphErrors, TF1, gStyle, kRed, kBlue, kGray, TFile, TTree, TPad, gPad
import csv
import matplotlib.pyplot as plt
from array import array
import numpy as np
import os.path
from scipy import stats
from scipy.optimize import curve_fit

# Setting up the ATLAS style
ROOT.gROOT.LoadMacro("/Users/jordi/code/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/Users/jordi/code/AtlasStyle/AtlasUtils.C")
ROOT.SetAtlasStyle()

def prepare_th2(th2,titlex, titley, titlez):
   #th2.SetTitleOffset(1.0)
   th2.GetXaxis().SetTitleOffset(1.3)
   #th2.GetZaxis().SetLabelSize(0.03);
   th2.GetZaxis().SetLabelSize(0.04)
   th2.GetXaxis().SetLabelSize(0.04)
   th2.GetYaxis().SetLabelSize(0.04)
   th2.GetXaxis().SetTitle(titlex)
   th2.GetYaxis().SetTitle(titley)
   th2.GetZaxis().SetTitle(titlez)


#model = "monolith2023"
model = "monolith2023_50umepi"

toa_list = []
toa_noise_list = []
amplitude_list = []
amplitude_noise_list = []
if model == "monolith2023":
    n_events = 10000
    amplitude_threshold = 3 # hardcoded threshold to avoid weird events
if model == "monolith2023_50umepi":
    n_events = 10000
    amplitude_threshold = 7 # hardcoded threshold to avoid weird events

sigma_jitter = []
sigma_jitter_2 = []
trise_list = []

# Set the seed for the random number generator
seed = 12345  # You can choose any seed value
randomGenerator = ROOT.TRandom3(seed)

#sigma_noise = 4.589  # mV
#sigma_noise = 0.57  # mV (~ experimental)
#sigma_noise = 0.26  # mV (simulation)
sigma_noise = 0.203  # mV (simulation more events)
#sigma_noise = 0.5*0.26  # 0.5 the noise from simulation
#sigma_noise = 4*0.26  # 2x the noise from simulation
six_sigma_threshold = 6*sigma_noise # mV
threshold = 7*sigma_noise # mV



xmin_amp = 0
if model == "monolith2023":
    #xmax_amp = 50
    xmax_amp = 80
if model == "monolith2023_50umepi":
    xmax_amp = 100

trise_hist = ROOT.TH1D("trise_hist", "trise;trise;Events", 100, 0, 0.5)  # You can adjust the number of bins and range
#amplitude_hist = ROOT.TH1D("amplitude_hist", "amplitude;amplitude;Events", 100, 0, 140)  # You can adjust the number of bins and range
amplitude_hist = ROOT.TH1D("amplitude_hist", "amplitude;Amplitude [mV];Events", 100, xmin_amp, xmax_amp)  # You can adjust the number of bins and range
amplitude_bins = 100


ymin_jitter = 0
ymax_jitter = 100
jitter_h2 = ROOT.TH2D("jitter_h2", "Jitter vs amplitude", amplitude_bins, xmin_amp, xmax_amp, amplitude_bins, ymin_jitter, ymax_jitter)


# plot the pulse of a given event
do_plot = False
do_fit_plot = False
event_to_plot = 34
# Replace 'your_file.csv' with the actual filename of your CSV file.
for iev in range(1,n_events+1):
    toa = 0
    toa_noise = 0
    toa_noise_before = 0
    toa_six_sigma = 0
    toa_six_sigma_noise = 0
    voltage_noise_before = 0
    voltage_noise_after = 0
    if model == "monolith2023":
        input_file = 'data_antonio/pulse_no_noise_parasitic_25psspacing/monolith2023_results_config_pulse'+str(iev)+'.csv'
    if model == "monolith2023_50umepi":
        input_file = 'data_antonio/pulse_50umepi_no_noise_parasitic_25psspacing/monolith2023_results_config_pulse'+str(iev)+'.csv'

    if not os.path.isfile(input_file):
        continue
    with open(input_file, 'r') as file:
        csv_reader = csv.reader(file)

        # Skip the header row if it exists
        next(csv_reader, None)
        
        # Initialize lists to store data from the CSV file
        x_values = []
        y_values = []
        y_noise_values = []
        

        # Read the data and store it in the lists
        #for row in csv_reader:
        for row_index, row in enumerate(csv_reader):

            if not row:
                break
            if row[0].isspace():
                continue
            x_value_tmp = float(row[0])*1e9 # in ns
            y_value_tmp = float(row[1])*1e3 # in mV
            x_values.append(x_value_tmp)
            y_values.append(y_value_tmp)

            # Define parameters for the Gaussian noise
            mean_noise = 0  # Mean of the Gaussian noise
            #smear = ROOT.gRandom.Gaus(mean_noise,sigma_noise)
            smear = randomGenerator.Gaus(mean_noise, sigma_noise)
            #y_value_noise_tmp = y_value_tmp * (1+smear)
            y_value_noise_tmp = y_value_tmp + smear
            y_noise_values.append(y_value_noise_tmp)

            # toa calculation
            #threshold = 4 # mV


            # toa defined as 7*sigma noise
            if y_value_tmp > threshold and toa == 0:
                toa = x_value_tmp
            
            # 6sigma toa to do the dV/dt fit
            if y_value_tmp > six_sigma_threshold and toa_six_sigma == 0:
                toa_six_sigma = x_value_tmp 

            # toa with noise
            if y_value_noise_tmp > threshold and toa_noise == 0:
                toa_noise = x_value_tmp
                toa_noise_before = x_values[row_index-1]
                voltage_noise_after = y_value_noise_tmp
                voltage_noise_before = y_noise_values[row_index-1]
                # no noise
                #voltage_noise_after = y_value_tmp
                #voltage_noise_before = y_values[row_index-1]
            # 6 sigma toa with noise to do the dV/dt fit
            if y_value_noise_tmp > six_sigma_threshold and toa_six_sigma_noise == 0:
                toa_six_sigma_noise = x_value_tmp 
                toa_six_sigma_noise_before = x_values[row_index-1]
                voltage_six_sigma_noise_after = y_value_noise_tmp
                voltage_six_sigma_noise_before = y_noise_values[row_index-1]
                # no noise
                #voltage_six_sigma_noise_after = y_value_tmp
                #voltage_six_sigma_noise_before = y_values[row_index-1]
                
    # remove emty events, for now removing maximum amplitudes < 1 mV as a proxy to spot empty events
    if max(y_values) < 1e-2:
        #print(max(y_values))
        continue

    # cut out weird events
    if max(y_values) < amplitude_threshold:
        continue

    

    # fit a straight line between the point before and after crossing the toa and calculate the exact point where it crossed it
    x_toa = np.array([toa_noise_before, toa_noise])
    y_voltage = np.array([voltage_noise_before, voltage_noise_after])
    # six sigma
    x_toa_six_sigma = np.array([toa_six_sigma_noise_before, toa_six_sigma_noise])
    y_voltage_six_sigma = np.array([voltage_six_sigma_noise_before, voltage_six_sigma_noise_after]) 
    # Fit a straight line (linear regression)
    # Check if both x and y are not all zeros
    m=0
    m_six_sigma=0
    n=0
    n_six_sigma=0
    # avoid empty events
    if not np.all(x_toa == 0) and not np.all(y_voltage == 0):
        m, n = np.polyfit(x_toa, y_voltage, 1)
        m_six_sigma, n_six_sigma = np.polyfit(x_toa_six_sigma, y_voltage_six_sigma, 1)
    else: 
        continue
    # Define a function to predict t for a given V
    # have to invert the function
    def predict_t(V_value,m,n):
        return  (V_value - n)/m
    
    toa_interpolated = predict_t(threshold,m,n)
    toa_interpolated_six_sigma = predict_t(six_sigma_threshold,m_six_sigma,n_six_sigma)

    #print(toa_noise,toa_interpolated)
    toa_list.append(toa)
    #toa_noise_list.append(toa_noise)
    toa_noise_list.append(toa_interpolated)

    #toa_list.append(toa)
    #toa_noise_list.append(toa_noise)

    amplitude = max(y_values)
    amplitude_list.append(amplitude)
    amplitude_noise = max(y_noise_values)
    amplitude_noise_list.append(amplitude_noise)



    ##### calculating jitter with ROOT like Matteo does #####
    # Create a ROOT TGraph to plot the waveform and be able to performa fit to it
    waveform_graph = ROOT.TGraph(len(x_values), array('d', x_values), array('d', y_values))
    axis = waveform_graph.GetXaxis()
    axis.SetRangeUser(0,1)
    # fit a linear regression between 6*sigma_noise to 7*sigma_noise to obtain dV/dt
    #waveform_canvas = ROOT.TCanvas("waveform canvas", "waveform canvas", 700, 600)
    #waveform_canvas.cd()
    #waveform_graph.Draw()
    #mylinear = ROOT.TF1("mylinear","pol1", toa_six_sigma_noise, toa_noise)

    #if (toa_interpolated_six_sigma > toa_interpolated):
    #    print(toa_interpolated_six_sigma,toa_interpolated)
    points_to_fit_y = np.array([toa_interpolated_six_sigma, toa_interpolated])
    points_to_fit_x = np.array([six_sigma_threshold, threshold])
    waveform_slope, waveform_n = np.polyfit(points_to_fit_y, points_to_fit_x, 1)
    #print(waveform_slope, waveform_n)

    #mylinear = ROOT.TF1("mylinear","pol1", toa_interpolated_six_sigma, toa_interpolated)
    #resultLinear = waveform_graph.Fit(mylinear, "SRN")
    #waveform_slope = resultLinear.Get().Parameter(1)
    #mylinear.Draw("SAME")
    # calculate the time jitter as sigma_noise/(dV/dt)
    time_jitter = (sigma_noise/waveform_slope)*1000 # in ps
    jitter_h2.Fill(amplitude_noise,time_jitter)




    # Generate x values for the line
    x_values_test = np.linspace(min(points_to_fit_y), max(points_to_fit_y), 100)

    # Calculate corresponding y values using the line equation
    y_values_test = waveform_slope * x_values_test + waveform_n



    if do_plot:
        #if iev != event_to_plot:
        #    continue

        #plt.figure(figsize=(8, 6))  # Adjust the figure size as needed
        plt.plot(x_values, y_values, marker='.', linestyle='-')  # Create a simple line plot with markers
        #plt.plot(x_values, noisy_signal)  # Create a simple line plot with markers
        #plt.plot(x_values, y_values)  # Create a simple line plot with markers
        plt.plot(x_values, y_noise_values)  # Create a simple line plot with markers
        plt.title('')
        plt.xlabel('Time [ns]')
        plt.ylabel('Amplitude [mV]')
        
        plt.grid(True)  # Add a grid to the plot if desired
        
        # Set the x and y-axis tick labels to scientific notation
        plt.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
        
        # Set the range of x-axis
        #plt.xlim(0, 10.)
        plt.xlim(0, 1)

        # Plot the straight line without specifying points
        plt.plot(x_values_test, y_values_test, color='red', label='Fitted Line')

        plt.show()  # Display the plot









    ### Calculating jitter in a different way ####
    # Define the range of x values you want to use for linear regression
    y_min = 0.2*amplitude # defining the minimum y to calculate dV/dt or rise time
    y_max = 0.8*amplitude # defining the maximum y to calculate dV/dt or rise time
    x_min = -1
    x_max = 10
    # loop over the times and find at which time the lower and upper value of the amplitude corresponds to
    for i in range(len(x_values)):
        if y_values[i] > y_min and x_min < 0:
            x_min = x_values[i-1] # we already passed the threshold, take the previous point
        if y_values[i] > y_max:
            x_max = x_values[i-1]
            break



    # dV/dt
    x_values = np.array(x_values)
    # Create a mask to filter the data within the specified range
    mask = (x_values >= x_min) & (x_values <= x_max)

    # Use the mask to select the subset of data
    x_subset = np.array(x_values)[mask]
    y_subset = np.array(y_values)[mask]

    # Perform linear regression on the subset of data
    slope, intercept, r_value, p_value, std_err = stats.linregress(x_subset, y_subset)

    # plot the fit
    if do_fit_plot:
        # Print the results
        print("Slope (m):", slope)
        print("Intercept (b):", intercept)
        print("R-squared value:", r_value**2)
        print("P-value:", p_value)
        print("Standard Error:", std_err)

        # Calculate the regression line
        regression_line = slope * x_subset + intercept

        # Plot the original data and the regression line
        plt.scatter(x_values, y_noise_values, label='Data')
        plt.plot(x_subset, regression_line, color='red', label='Linear Regression')

        # Add labels and a legend
        plt.xlabel('X Values')
        plt.ylabel('Y Values')
        plt.legend()
        plt.xlim(0, 1.)

        # Display the plot
        plt.show()
    

    # sigma_jitter = N/(dV/dt) 
    sigma_jitter.append(sigma_noise/slope)


    # Now calculate the jitter as jitter = trise / (S/N) = N * trise / S
    trise = x_max - x_min
    trise_list.append(trise)
    trise_hist.Fill(trise)
    sigma_jitter_2.append(sigma_noise * trise / amplitude)
    #print(sigma_noise/slope,sigma_noise * trise / amplitude)
    amplitude_hist.Fill(amplitude)

'''
print("fitting gaussian")
# fit trise to a gaussian
trise_canvas = ROOT.TCanvas("trise canvas", "trise canvas", 700, 600)
trise_canvas.cd()
trise_hist.Draw()
print(trise_hist.GetMean() - 2. * trise_hist.GetRMS(), trise_hist.GetMean() + 2. * trise_hist.GetRMS())
myfunPre = ROOT.TF1("fitPre","gaus", trise_hist.GetMean() - 2. * trise_hist.GetRMS(), trise_hist.GetMean() + 2. * trise_hist.GetRMS())
resultPre = trise_hist.Fit(myfunPre, "SRQN")
myfun = ROOT.TF1("finalGaus", "gaus", resultPre.Get().Parameter(1) - 2. * resultPre.Get().Parameter(2), resultPre.Get().Parameter(1) + 2. * resultPre.Get().Parameter(2) )
result = trise_hist.Fit(myfun, "SRQN")
trise_mean = result.Get().Parameter(1)
trise_sigma = result.Get().Parameter(2)
print("mean:", round(trise_mean,2), "[GeV]")
print("sigma:", round(trise_sigma  ,2), "[GeV]")
myfun.Draw("SAME")
trise_canvas.Draw()
trise_canvas.SaveAs("trise.pdf")

'''

print("fitting landau")
# fit amplitude to a landau
amplitude_canvas = ROOT.TCanvas("amplitude canvas", "amplitude canvas", 700, 600)
amplitude_canvas.cd()
amplitude_hist.Draw()
mylandau = ROOT.TF1("mylandau","landau", 0, xmax_amp)
# set an initial parameter for the MPV term so the fit converges
mylandau.SetParameter(1,amplitude_hist.GetMean())
resultLandau = amplitude_hist.Fit(mylandau, "SQRN")
amplitude_MPV = resultLandau.Get().Parameter(1)
amplitude_MPV_err = resultLandau.Get().ParError(1)
chi2_landau = mylandau.GetChisquare()
ndf_landau = mylandau.GetNDF()
ROOT.myText(0.6,  0.88, 1, "MPV = "+str(round(amplitude_MPV,1))+" +/- "+str(round(amplitude_MPV_err,1)),0.035)
ROOT.myText(0.6,  0.80, 1, "#chi^{2}/ndf = "+str(round(chi2_landau,1))+'/'+str(round(ndf_landau,1)),0.035)
mylandau.Draw("SAME")
amplitude_canvas.SaveAs("plots/amplitude_"+model+".pdf")
#print('jitter contribution =',sigma_noise * trise_mean / amplitude_MPV, 'ns')
print('Amplitude MPV',amplitude_MPV)

# jitter canvas
# Plotting the time jitter as a function of the aplitude and taking the profile of the distribution
# Then we take the MPV of the amplitudes (previous plot) and we see to which time jitter this amplitude corresponds to
print('Preparing profile canvas')
prof_x_time_jitter = jitter_h2.ProfileX()
prof_x_time_jitter.SetMarkerSize(0.8)
time_jitter_canvas = ROOT.TCanvas("time_jitter canvas", "time_jitter canvas", 700, 600)
time_jitter_canvas.cd()
gPad.SetRightMargin(0.17)
prepare_th2(jitter_h2, "Amplitude [mV]", "Time jitter [ps]", "Events")
jitter_h2.Draw("colz")
prof_x_time_jitter.Draw("same")
# fitting a 1/x function to the jitter vs amplitude profile
oneOverx_fit = ROOT.TF1("oneOverx_fit", "[0]/x+[1] + x*[2]", xmin_amp, xmax_amp)
# set an initial parameter for the constant term so the fit converges
oneOverx_fit.SetParameter(1,prof_x_time_jitter.GetBinContent(prof_x_time_jitter.GetNbinsX()))
oneOverx_fit_result = prof_x_time_jitter.Fit(oneOverx_fit,"S")
oneOverx_fit.Draw("same")
final_time_jitter = oneOverx_fit.Eval(amplitude_MPV)
print('Final time jitter contribution = ',final_time_jitter)
time_jitter_canvas.SaveAs("plots/jitter_vs_amp_"+model+".pdf")


# save the histograms in a root file
output_file = ROOT.TFile("output/"+model+"_jitter.root", "RECREATE")
prof_x_time_jitter.Write()
amplitude_hist.Write()
output_file.Close()