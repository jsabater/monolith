import ROOT
import csv
import matplotlib.pyplot as plt
from array import array
import numpy as np
import os.path

toa_list = []
toa_noise_list = []
amplitude_list = []
amplitude_noise_list = []
n_events = 10000
#n_events = 8794

#sigma_noise = 4.589  # mV
sigma_noise = 0.57  # mV (~ experimental)
#sigma_noise = 0.26  # mV (simulation)
#sigma_noise = 0.203  # mV (simulation more events)
#sigma_noise = 2*sigma_noise  # mV 2x the noise from simulation
#sigma_noise = 4*sigma_noise  # mV 4x the noise from simulation
threshold = 7*sigma_noise # mV

# Set the seed for the random number generator
seed = 12345  # You can choose any seed value
randomGenerator = ROOT.TRandom3(seed)



# plot the pulse of a given event
do_plot = False
event_to_plot = 19
# Replace 'your_file.csv' with the actual filename of your CSV file.
for iev in range(1,n_events+1):
    toa = 0
    toa_before = 0
    toa_noise = 0
    toa_noise_before = 0
    voltage_before = 0
    voltage_after = 0
    voltage_noise_before = 0
    voltage_noise_after = 0
    input_file = 'data_antonio/pulse_no_noise_parasitic_25psspacing/monolith2023_results_config_pulse'+str(iev)+'.csv'
    #input_file = 'data_antonio/pulse_50umepi_no_noise_parasitic_25psspacing/monolith2023_results_config_pulse'+str(iev)+'.csv'
    #input_file = 'data_antonio/pulse_no_noise_parasitic_1psspacing/monolith2023_results_config_pulse'+str(iev)+'.csv'
    if not os.path.isfile(input_file):
        continue
    with open(input_file, 'r') as file:
        csv_reader = csv.reader(file)

        # Skip the header row if it exists
        next(csv_reader, None)
        
        # Initialize lists to store data from the CSV file
        x_values = []
        y_values = []
        y_noise_values = []
        

        # Read the data and store it in the lists
        #for row in csv_reader:
        for row_index, row in enumerate(csv_reader):
            if not row:
                break
            if row[0].isspace():
                continue
            x_value_tmp = float(row[0])*1e9 # in ns
            y_value_tmp = float(row[1])*1e3 # in mV
            x_values.append(x_value_tmp)
            y_values.append(y_value_tmp)

            # Define parameters for the Gaussian noise
            mean_noise = 0  # Mean of the Gaussian noise
            #smear = ROOT.gRandom.Gaus(mean_noise,sigma_noise)
            smear = randomGenerator.Gaus(mean_noise, sigma_noise)
            #print(smear)
            #y_value_noise_tmp = y_value_tmp * (1+smear)
            y_value_noise_tmp = y_value_tmp + smear
            y_noise_values.append(y_value_noise_tmp)

            # toa calculation
            if y_value_tmp > threshold and toa == 0:
                toa = x_value_tmp
                toa_before = x_values[row_index-1]
                voltage_after = y_value_tmp
                voltage_before = y_values[row_index-1]
            # toa with noise
            if y_value_noise_tmp > threshold and toa_noise == 0:
                toa_noise = x_value_tmp
                toa_noise_before = x_values[row_index-1]
                voltage_noise_after = y_value_noise_tmp
                voltage_noise_before = y_noise_values[row_index-1]
                


    # remove emty events, for now removing maximum amplitudes < 1 mV as a proxy to spot empty events
    if max(y_values) < 1e-2:
        #print(max(y_values))
        continue

    #plot_flag = False
    #if max(y_values) < 10:
    #    print('amplitude < 10 for event ',iev, ' amplitude = ',max(y_values))
    #    plot_flag = True

    # fit a straight line between the point before and after crossing the toa and calculate the exact point where it crossed it
    # without noise
    x_toa = np.array([toa_before, toa]) 
    y_voltage = np.array([voltage_before, voltage_after])  # Replace y1 and y2 with your y-values
    # w/ noise
    x_toa_noise = np.array([toa_noise_before, toa_noise]) 
    y_voltage_noise = np.array([voltage_noise_before, voltage_noise_after])  # Replace y1 and y2 with your y-values
    # Fit a straight line (linear regression)
    # Check if both x and y are not all zeros
    m = 0
    n = 0
    m_noise = 0
    n_noise = 0
    # avoid empty events
    if not np.all(x_toa == 0) and not np.all(y_voltage == 0):
        m, n = np.polyfit(x_toa, y_voltage, 1)
    if not np.all(x_toa_noise == 0) and not np.all(y_voltage_noise == 0):
        m_noise, n_noise = np.polyfit(x_toa_noise, y_voltage_noise, 1)
    else: 
        continue
    # Define a function to predict t for a given V
    # have to invert the function
    def predict_t(V_value,m,n):
        if m != 0:
            return  (V_value - n)/m
        else:
            return 0
    
    toa_interpolated = predict_t(threshold,m,n)
    toa_noise_interpolated = predict_t(threshold,m_noise,n_noise)
    #print(toa_noise,toa_noise_interpolated)
    toa_list.append(toa_interpolated)
    #toa_noise_list.append(toa_noise)
    toa_noise_list.append(toa_noise_interpolated)

    amplitude = max(y_values)
    amplitude_list.append(amplitude)
    amplitude_noise = max(y_noise_values)
    amplitude_noise_list.append(amplitude_noise)


    #print(amplitude,toa)

        

    # Generate Gaussian noise
    #noise = np.random.normal(mean_noise, sigma_noise, len(x_values))
    # Add noise to the signal
    #noisy_signal = y_values + noise

    if do_plot:
        #if iev != event_to_plot:
        #    continue
        #if not plot_flag:
        #    continue

        #plt.figure(figsize=(8, 6))  # Adjust the figure size as needed
        plt.plot(x_values, y_values, marker='.', linestyle='-')  # Create a simple line plot with markers
        #plt.plot(x_values, noisy_signal)  # Create a simple line plot with markers
        #plt.plot(x_values, y_values)  # Create a simple line plot with markers
        plt.plot(x_values, y_noise_values)  # Create a simple line plot with markers
        plt.title('')
        plt.xlabel('Time [ns]')
        plt.ylabel('Amplitude [mV]')
        
        plt.grid(True)  # Add a grid to the plot if desired
        
        # Set the x and y-axis tick labels to scientific notation
        plt.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
        
        # Set the range of x-axis
        plt.xlim(0, 10.)
        #plt.xlim(0, 2.)
        plt.show()  # Display the plot




#print(amplitude_list)
#print(toa_list)



# Create a ROOT file to store the TTree
root_file = ROOT.TFile("toa.root", "RECREATE")
#root_file = ROOT.TFile("toa_100pA.root", "RECREATE")

# Create a TTree
tree = ROOT.TTree("myTree", "My Tree")

# Create variables to store your data
eventID = array('i', [0])
seed_amplitude = array('d', [0])
seed_amplitude_noise = array('d', [0])
seed_toa = array('d', [0])
seed_toa_noise = array('d', [0])

# Create branches with the variables
tree.Branch("eventID", eventID, "eventID/I")
tree.Branch("seed_amplitude", seed_amplitude, "seed_amplitude/D")
tree.Branch("seed_amplitude_noise", seed_amplitude_noise, "seed_amplitude_noise/D")
tree.Branch("seed_toa", seed_toa, "seed_toa/D")
tree.Branch("seed_toa_noise", seed_toa_noise, "seed_toa_noise/D")

# Fill the TTree with data from your lists
for i in range(len(amplitude_list)):
    eventID[0] = i+1
    seed_amplitude[0] = amplitude_list[i]
    seed_amplitude_noise[0] = amplitude_noise_list[i]
    seed_toa[0] = toa_list[i]
    seed_toa_noise[0] = toa_noise_list[i]
    tree.Fill()

# Write the TTree to the ROOT file
tree.Write()

# Close the ROOT file
root_file.Close()
